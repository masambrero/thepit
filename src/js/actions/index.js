import fetch from 'isomorphic-fetch';
import * as actionTypes from 'Constants/ActionTypes';

export const setTenseFilter = filter => ({
  type: actionTypes.SET_TENSE_FILTER,
  filter,
});

export const requestTasks = filter => ({
  type: actionTypes.REQUEST_TASKS,
  filter,
});

export const receiveTasks = json => ({
  type: actionTypes.RECEIVE_TASKS,
  tasks: json,
});

export const repeatTasks = () => ({
  type: actionTypes.REPEAT_TASKS,
});

export const resetTasks = () => ({
  type: actionTypes.RESET_TASKS,
});

export const showErrorModal = error => ({
  type: actionTypes.SHOW_ERROR_MODAL,
  error,
});

export const hideErrorModal = () => ({
  type: actionTypes.HIDE_ERROR_MODAL,
});

export const getNextTask = () => ({
  type: actionTypes.GET_NEXT_TASK,
});

export const getPrevTask = () => ({
  type: actionTypes.GET_PREV_TASK,
});

const fetchTasks = filter => (dispatch, getState) => {
  const offset = getState().tasks.offset;
  dispatch(requestTasks(filter));
  return fetch(
    '/api/english/get_tasks', {
      method: 'post',
      mode: 'same-origin',
      body: JSON.stringify({
        filter,
        offset,
      }),
    })
    .then((response) => {
      if (response.status !== 200) {
        return false;
      }
      return response.json();
    })
    .then((json) => {
      if (json.length === 0) dispatch(showErrorModal('No tasks found.')); dispatch(receiveTasks(json));
    })
    .catch((error) => {
      dispatch(showErrorModal(error));
    },
  );
};

const shouldFetchTasks = (state) => {
  const tasks = state.tasks.nextTasks.length;
  if (state.isFetching) {
    return false;
  }
  if (tasks <= 1) {
    return true;
  }
  return false;
};

export const fetchTasksIfNeeded = filter => (dispatch, getState) => (
  shouldFetchTasks(getState()) ? dispatch(fetchTasks(filter)) : false
);

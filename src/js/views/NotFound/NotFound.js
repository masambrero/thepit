import React from 'react';

const NotFound = () => (
  // We add content__page class for every route link
  // for correct transition animation
  <div className="content__page">
    <div className="notFoundTab content__padding">
      <p>Page not found. We are sorry.</p>
      <div className="notFoundTab__img" />
    </div>
  </div>
);

export default NotFound;

import React from 'react';
import './Home.css';

const Home = () => (
  <div className="thepitLogo">
    <div className="thepitLogo__subheader">THE</div>
    <div className="thepitLogo__header">PIT</div>
  </div>
);

export default (Home);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import whyDidYouUpdate from 'why-did-you-update';
import {
  fetchTasksIfNeeded, setTenseFilter,
  getPrevTask, getNextTask,
  repeatTasks, resetTasks,
} from 'Actions';
import Loader from 'Components/SVG/Loader';
import TenseTab from 'Components/TenseTab/TenseTab';
import TaskTab from 'Components/TaskTab/TaskTab';

import './English.css';

// if (process.env.NODE_ENV !== 'production') {
//   whyDidYouUpdate(React);
// }

class English extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tenseTabShown: false,
      taskTabShown: false,
      translationShown: false,
    };
    this.toggleTenseTab = this.toggleTenseTab.bind(this);
    this.changeFilter = this.changeFilter.bind(this);
    this.onTranslateButtonClick = this.onTranslateButtonClick.bind(this);
    this.onRepeatButtonClick = this.onRepeatButtonClick.bind(this);
    this.onPrevButtonClick = this.onPrevButtonClick.bind(this);
    this.onNextButtonClick = this.onNextButtonClick.bind(this);
    this.hideTranslation = this.hideTranslation.bind(this);
  }

  onTranslateButtonClick() {
    this.setState({ translationShown: !this.state.translationShown });
  }

  onRepeatButtonClick() {
    this.hideTranslation();
    this.props.repeatTasks();
    this.props.getNextTask();
  }

  onPrevButtonClick() {
    this.hideTranslation();
    this.props.getPrevTask();
  }

  onNextButtonClick() {
    const savedFilter = this.props.savedFilter;
    this.hideTranslation();
    this.props.fetchTasksIfNeeded(savedFilter);
    this.props.getNextTask();
  }

  changeFilter(filter) {
    const savedFilter = this.props.savedFilter;
    if (filter.sort().toString() !== savedFilter.sort().toString()) {
      this.props.setTenseFilter(filter);
      this.props.resetTasks();
    }
    this.setState({
      tenseTabShown: !this.state.tenseTabShown,
    });
    this.props.fetchTasksIfNeeded(filter);
  }

  toggleTenseTab() {
    this.setState({
      tenseTabShown: !this.state.tenseTabShown,
    });
  }

  hideTranslation() {
    this.setState({ translationShown: false });
  }

  render() {
    const { isFetching } = this.props;
    const { tenseTabShown, translationShown } = this.state;
    const taskTabShown = !!this.props.currentTask;
    return (
      <div className="english-task">
        <div
          className={`loader ${isFetching ? 'loader--shown' : 'loader--hidden'}`}
        >
          <div className="loader__icon">
            <Loader />
          </div>
        </div>
        <div className="english-task__info-field">
          <header className="english-task__header">
            ENGLISH<div className="english-task__subheader">exercises</div>
          </header>
          <div className="english-task__filters">
            <p>Выберите тему:</p>
            <button
              className="btn"
              onClick={this.toggleTenseTab}
            >
              выбрать
            </button>
          </div>
        </div>
        <div className="english-task__task-field">
          <TenseTab
            tenseTabShown={tenseTabShown}
            filters={this.props.filtersById}
            changeFilter={this.changeFilter}
          />
          <TaskTab
            taskTabShown={taskTabShown}
            translationShown={translationShown}
            taskInfo={this.props.tasksById[this.props.currentTask]}
            onTranslateButtonClick={this.onTranslateButtonClick}
            onPrevButtonClick={this.onPrevButtonClick}
            onRepeatButtonClick={this.onRepeatButtonClick}
            onNextButtonClick={this.onNextButtonClick}
            canPrev={this.props.canPrev}
            canNext={this.props.canNext}
          />
        </div>
      </div>
    );
  }
}

English.defaultProps = {
  currentTask: null,
  savedFilter: [],
  tasksById: {},
};

English.propTypes = {
  repeatTasks: PropTypes.func.isRequired,
  fetchTasksIfNeeded: PropTypes.func.isRequired,
  getNextTask: PropTypes.func.isRequired,
  setTenseFilter: PropTypes.func.isRequired,
  resetTasks: PropTypes.func.isRequired,
  getPrevTask: PropTypes.func.isRequired,
  canPrev: PropTypes.bool.isRequired,
  canNext: PropTypes.bool.isRequired,
  currentTask: PropTypes.number,
  savedFilter: PropTypes.arrayOf(PropTypes.number.isRequired),
  isFetching: PropTypes.bool.isRequired,
  filtersById: PropTypes.shape({
    [PropTypes.number.isRequired]: PropTypes.shape({
      id: PropTypes.number.isRequired,
      ru_text: PropTypes.string.isRequired,
      en_text: PropTypes.string.isRequired,
      description: PropTypes.string,
    }),
  }).isRequired,
  tasksById: PropTypes.shape({
    [PropTypes.number.isRequired]: PropTypes.shape({
      id: PropTypes.number.isRequired,
      ru_text: PropTypes.string.isRequired,
      en_text: PropTypes.string.isRequired,
      description: PropTypes.string,
    }),
  }),
};

const mapStateToProps = state => ({
  isFetching: state.tasks.isFetching,
  savedFilter: state.tenseFilter.savedFilter,
  filtersById: state.tenseFilter.filtersById,
  currentTask: state.tasks.currentTask,
  tasksById: state.tasks.tasksById,
  canPrev: !!state.tasks.prevTasks.length,
  canNext: !!state.tasks.nextTasks.length,
});

const mapDispatchToProps = dispatch => ({
  setTenseFilter: bindActionCreators(setTenseFilter, dispatch),
  fetchTasksIfNeeded: bindActionCreators(fetchTasksIfNeeded, dispatch),
  getPrevTask: bindActionCreators(getPrevTask, dispatch),
  repeatTasks: bindActionCreators(repeatTasks, dispatch),
  getNextTask: bindActionCreators(getNextTask, dispatch),
  resetTasks: bindActionCreators(resetTasks, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(English);

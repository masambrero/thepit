import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { hideErrorModal } from 'Actions';
import './Modal.css';

class Modal extends Component {
  componentDidMount() {
    this.toggleBodyOverflow();
  }

  componentWillUnmount() {
    this.toggleBodyOverflow();
  }

  toggleBodyOverflow = () => {
    document.body.classList.toggle('modal-open');
  }

  render() {
    return (
      <div className="modal" >
        <div className="modal__overlay" />
        <div className="modal__info">
          <div className="modal-content">
            <div className="modal-content__header modal-header">
              <div className="modal-header__subheader">status</div>
              <h2 className="modal-header__header">error</h2>
              <button
                className="modal-header__btn" onClick={this.props.hideErrorModal}
              >
                &#9932;
              </button>
            </div>
            <div className="modal-content__body">
              {this.props.modalInfo}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.defaultProps = {
  modalInfo: null,
};

Modal.propTypes = {
  hideErrorModal: PropTypes.func.isRequired,
  modalInfo: PropTypes.string,
};

const mapStateToProps = state => ({
  modalInfo: state.modal.modalInfo,
});

const mapDispatchToProps = dispatch => ({
  hideErrorModal: bindActionCreators(hideErrorModal, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Modal);

import React from 'react';

const Loader = () => (
  <svg
    width="150"
    height="150"
    viewBox="0 0 150 150"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g
      fill="none"
      fillRule="evenodd"
      transform="translate(1 1)"
      strokeWidth="2"
    >
      <circle cx="75" cy="75" r="24" strokeOpacity="0">
        <animate
          attributeName="r"
          begin="1.5s" dur="3s"
          values="24;75"
          calcMode="linear"
          repeatCount="indefinite"
        />
        <animate
          attributeName="stroke-opacity"
          begin="1.5s" dur="3s"
          values="1;0" calcMode="linear"
          repeatCount="indefinite"
        />
        <animate
          attributeName="stroke-width"
          begin="1.5s" dur="3s"
          values="2;0" calcMode="linear"
          repeatCount="indefinite"
        />
      </circle>
      <circle cx="75" cy="75" r="24" strokeOpacity="0">
        <animate
          attributeName="r"
          begin="3s" dur="3s"
          values="24;75"
          calcMode="linear"
          repeatCount="indefinite"
        />
        <animate
          attributeName="stroke-opacity"
          begin="3s" dur="3s"
          values="1;0" calcMode="linear"
          repeatCount="indefinite"
        />
        <animate
          attributeName="stroke-width"
          begin="3s" dur="3s"
          values="2;0" calcMode="linear"
          repeatCount="indefinite"
        />
      </circle>
      <circle cx="75" cy="75" r="28">
        <animate
          attributeName="r"
          begin="0s" dur="1.5s"
          values="24;8;12;16;20;24"
          calcMode="linear"
          repeatCount="indefinite"
        />
      </circle>
    </g>
  </svg>
);

export default Loader;

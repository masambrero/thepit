import React from 'react';
import PropTypes from 'prop-types';
import './Task.css';

const Task = ({ taskInfo, translationShown }) => (
  <div className="task-tab__item">
    <div className="task-item">
      {!translationShown &&
        <div className="task-item__ru-text">
          <span className="task-item__lang">ru</span>
          {taskInfo.ru_text}
        </div>
      }
      {translationShown &&
        <div className="task-item__en-text">
          <span className="task-item__lang">en</span>
          {taskInfo.en_text}
          <div className="task-item__description">{taskInfo.description}</div>
        </div>
      }
    </div>
  </div>
);

Task.propTypes = {
  taskInfo: PropTypes.shape({
    ru_text: PropTypes.string.isRequired,
    en_text: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  translationShown: PropTypes.bool.isRequired,
};

export default Task;

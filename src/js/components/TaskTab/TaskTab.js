import React from 'react';
import PropTypes from 'prop-types';
import Task from 'Components/Task/Task';
import TaskButtons from 'Components/TaskButtons/TaskButtons';
import './TaskTab.css';

const TaskTab = ({
  taskTabShown,
  translationShown,
  taskInfo,
  onTranslateButtonClick,
  onPrevButtonClick,
  onRepeatButtonClick,
  onNextButtonClick,
  canPrev,
  canNext,
}) => (
  <div>
    {taskTabShown &&
      <div className="task-tab">
        <div>
          <Task
            translationShown={translationShown}
            taskInfo={taskInfo}
          />
          <TaskButtons
            onTranslateButtonClick={onTranslateButtonClick}
            onPrevButtonClick={onPrevButtonClick}
            onRepeatButtonClick={onRepeatButtonClick}
            onNextButtonClick={onNextButtonClick}
            canPrev={canPrev}
            canNext={canNext}
          />
        </div>
      </div>
    }
  </div>
);


TaskTab.defaultProps = {
  taskInfo: {},
};

TaskTab.propTypes = {
  taskTabShown: PropTypes.bool.isRequired,
  translationShown: PropTypes.bool.isRequired,
  taskInfo: PropTypes.shape({
    ru_text: PropTypes.string,
    en_text: PropTypes.string,
    description: PropTypes.string,
  }),
  onTranslateButtonClick: PropTypes.func.isRequired,
  onPrevButtonClick: PropTypes.func.isRequired,
  onRepeatButtonClick: PropTypes.func.isRequired,
  onNextButtonClick: PropTypes.func.isRequired,
  canPrev: PropTypes.bool.isRequired,
  canNext: PropTypes.bool.isRequired,
};

export default TaskTab;

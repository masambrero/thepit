import React from 'react';
import PropTypes from 'prop-types';
import Arrow from 'Components/SVG/Arrow';
import Refresh from 'Components/SVG/Refresh';
import './TaskButtons.css';

const TaskButtons = ({
  onTranslateButtonClick,
  onPrevButtonClick,
  onRepeatButtonClick,
  onNextButtonClick,
  canPrev,
  canNext }) => (
    <div className="taskTab__buttonTab">
      <button
        type="button"
        className="taskTab__button taskTab__button--translate btn"
        onClick={onTranslateButtonClick}
      >
        Перевести
      </button>
      <button
        type="button"
        className="taskTab__button taskTab__button--prev taskTab__button--nav btn"
        onClick={onPrevButtonClick}
        disabled={!canPrev}
      >
        <Arrow />
      </button>
      <button
        type="button"
        className="taskTab__button taskTab__button--refresh taskTab__button--nav btn"
        onClick={onRepeatButtonClick}
      >
        <Refresh />
      </button>
      <button
        type="button"
        className="taskTab__button taskTab__button--next taskTab__button--nav btn"
        onClick={onNextButtonClick}
        disabled={!canNext}
      >
        <Arrow />
      </button>
    </div>
);

TaskButtons.propTypes = {
  onTranslateButtonClick: PropTypes.func.isRequired,
  onPrevButtonClick: PropTypes.func.isRequired,
  onRepeatButtonClick: PropTypes.func.isRequired,
  onNextButtonClick: PropTypes.func.isRequired,
  canPrev: PropTypes.bool.isRequired,
  canNext: PropTypes.bool.isRequired,
};

export default TaskButtons;

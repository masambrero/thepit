import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './TenseTab.css';

class TenseTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentFilter: [],
    };
    this.toggleTense = this.toggleTense.bind(this);
  }

  shouldComponentUpdate(prevProps, prevState) {
    return this.props.filters !== prevProps.filters ||
      this.props.tenseTabShown !== prevProps.tenseTabShown ||
      this.state.currentFilter !== prevState.currentFilter;
  }

  toggleTense(event) {
    let newFilter;
    const currentFilter = this.state.currentFilter;
    const index = currentFilter.indexOf(+event.target.value);
    if (event.target.checked) {
      newFilter = [...currentFilter, +event.target.value];
    } else {
      newFilter = currentFilter.filter(i => i !== currentFilter[index]);
    }
    this.setState({ currentFilter: newFilter });
  }

  render() {
    const { filters, changeFilter, tenseTabShown } = this.props;
    return (
      <div
        className={`tense-tab
        ${tenseTabShown ? 'tense-tab--shown' : 'tense-tab--hidden'}`}
      >
        <div className="tense-tab__list">
          {Object.values(filters).map(i => (
            <div className="tense-tab__item" key={i.id}>
              <input
                type="checkbox"
                className="tense-tab__input"
                id={`tense${i.id}`}
                value={i.id}
                onChange={this.toggleTense}
                checked={this.state.currentFilter.includes(i.id)}
              />
              <label
                className="tense-tab__label"
                htmlFor={`tense${i.id}`}
              >
                {i.name}
              </label>
            </div>
          ))}
          <button
            className="btn"
            onClick={() => changeFilter(this.state.currentFilter)}
          >
            Установить
          </button>
        </div>
      </div>
    );
  }
}

TenseTab.propTypes = {
  tenseTabShown: PropTypes.bool.isRequired,
  filters: PropTypes.shape({
    [PropTypes.number.isRequired]: PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }),
  }).isRequired,
  changeFilter: PropTypes.func.isRequired,
};

export default TenseTab;

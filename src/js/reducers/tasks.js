import {
  REQUEST_TASKS, RECEIVE_TASKS,
  GET_NEXT_TASK, GET_PREV_TASK,
  REPEAT_TASKS, RESET_TASKS,
} from 'Constants/ActionTypes';

const initialState = {
  isFetching: false,
  nextTasks: [],
  prevTasks: [],
  offset: 0,
};

const tasks = (state = initialState, action) => {
  let nextTasks;
  let currentTask;
  let prevTasks;
  const tasksById = {};
  switch (action.type) {
    case REQUEST_TASKS:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_TASKS:
      if (!action.tasks.length) {
        return {
          ...state,
          isFetching: false,
        };
      }
      action.tasks.map((i, index) => (tasksById[action.tasks[index].id] = i));
      nextTasks = [
        ...state.nextTasks,
        ...action.tasks.map(i => i.id),
      ];
      prevTasks = state.currentTask
        ? [...state.prevTasks, state.currentTask]
        : [...state.prevTasks];
      currentTask = nextTasks.shift();
      return {
        ...state,
        isFetching: false,
        tasksById: { ...state.tasksById, ...tasksById },
        offset: state.offset + action.tasks.length,
        currentTask,
        nextTasks,
        prevTasks,
      };
    case GET_NEXT_TASK:
      return {
        ...state,
        prevTasks: [...state.prevTasks, state.currentTask],
        currentTask: state.nextTasks.slice(0, 1).shift(),
        nextTasks: state.nextTasks.slice(1, state.nextTasks.length),
      };
    case GET_PREV_TASK:
      return {
        ...state,
        prevTasks: state.prevTasks.slice(1, state.prevTasks.length),
        currentTask: state.prevTasks.slice(0, 1).shift(),
        nextTasks: [state.currentTask, ...state.nextTasks],
      };
    case REPEAT_TASKS:
      return {
        ...state,
        isFetching: false,
        nextTasks: [...state.prevTasks, ...state.nextTasks],
        prevTasks: [],
      };
    case RESET_TASKS:
      return {
        ...state,
        nextTasks: [],
        prevTasks: [],
        offset: 0,
      };
    default:
      return state;
  }
};

export default tasks;

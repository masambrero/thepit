import {
  SHOW_ERROR_MODAL, HIDE_ERROR_MODAL,
} from 'Constants/ActionTypes';

const initialState = {
  modalShown: false,
  modalInfo: null,
};

const modal = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_ERROR_MODAL:
      return {
        ...state,
        modalShown: true,
        modalInfo: action.error,
      };
    case HIDE_ERROR_MODAL:
      return initialState;
    default:
      return state;
  }
};

export default modal;

import { combineReducers } from 'redux';
import modal from './modal';
import tasks from './tasks';
import tenseFilter from './tenseFilter';

const rootReducer = combineReducers({
  modal,
  tasks,
  tenseFilter,
});

export default rootReducer;

import { SET_TENSE_FILTER } from 'Constants/ActionTypes';

const initialState = {
  currentFilter: [],
  filtersById: {
    1: {
      id: 1,
      name: 'Main Tenses',
    },
    2: {
      id: 2,
      name: 'Rare Tenses',
    },
    3: {
      id: 3,
      name: 'Complex Object',
    },
  },
};

const tenseFilter = (state = initialState, action) => {
  switch (action.type) {
    case SET_TENSE_FILTER:
      return { ...state, savedFilter: action.filter };
    default:
      return state;
  }
};

export default tenseFilter;

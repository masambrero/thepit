package myproject;
use Dancer2;
use Dancer2::Plugin::Database;
use Data::Dumper qw/ Dumper /;
use common::sense;

set serializer => 'JSON';
set show_errors => 1;

our $VERSION = '0.1';

any '/api/english/get_tasks' => sub {
    my $dbh = database({ driver => 'SQLite', database => 'lib/test_project.db' });
    my $params = from_json(request->body);
    my $tasks = [database->quick_select('tasks',
      { tence_id => $params->{filter} },
      { order_by => 'tence_id', offset => $params->{offset}, limit => 10 }
    )];
    return $tasks;
};

true;

module.exports = {
  "extends": "eslint-config-airbnb",
  "env": {
    "browser": true
  },
  "rules": {
    "max-len": "off",
    "react/jsx-filename-extension": ["error", {"extensions": [".js"]}],
    "import/no-extraneous-dependencies": ["error", {"devDependencies": true}]
  },
  "parser": "babel-eslint",
  "plugins": [
    "react"
  ],
  "settings": {
    "import/resolver": {
      "webpack": { "config": "webpack.config.js" }
    }
  }
};
